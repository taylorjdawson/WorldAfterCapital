# Needs

The definition of scarcity that I just introduced is based on the notion of needs. To argue that there is a shift in scarcity from capital to attention thus requires an agreed upon set of needs to show that we indeed have sufficient capital. Can we make progress in defining what constitutes a set of basic human needs?

I am not proposing that this is a simple task. What follows should be considered a way of starting a dialog. A list of basic needs is a piece of knowledge. As such it can be improved over time through the process of critical inquiry. You can critique my list by pointing out flaws, you can also propose changes to my list, or you can publish your own list altogether.

One of the benefits of my approach to writing World After Capital out in the open and with revisions tracked is that you can see how my thinking on needs has evolved over time. In an earlier version I tried to group needs into categories such as biological, physical, and social. But the boundaries between those seemed rather arbitrary upon further examination. So in the current version I am distinguishing only between individual and collective needs, where the former will apply to any human wherever they may be (even say traveling in space) and the latter are the needs of humanity as a whole.

Another challenge in putting together a list of needs is that it is all too easy to confuse a need with a strategy for meeting this need. For instance, having a home that you own is a strategy for solving the need for shelter, but so would be access to shared space. 


## Individual Needs

These are the basic needs of the human body and mind. Without them individual survival and flourishing is impossible.

Oxygen. Humans need on average about 0.55 cubic meters of oxygen per day. The exact need of course varies with factors such as the size of our respective body and the degree of physical exertion. Our most common solution to this need is breathing air.

Water. We need to drink on average about 2.5 liters of water per day to stay hydrated. Again various factors such as body size, exertion and temperature will affect the exact need.

Energy. To power our bodies we require about 2,000 calories per day. We solve this need by consuming food. The best way to do this, however, is surprisingly controversial and poorly understood for such a basic need. In particular, the degree to which we have a need for fats versus carbohydrate intake.

Nutrients. The body cannot synthesize all the vitamins and minerals that it requires. Therefore some of them must be obtained directly as part of our nutrition. This too is an area that is surprisingly poorly understood, meaning which nutrients exactly we really need to acquire externally seems unsettled. There is a wide range of food consumption strategies that seem to support the human body.

(To research: does the body need light? It does for Vitamin D synthesis but we could also ingest Vitamin D directly)

Discharge. So this may be a bit gross but we also need to get things out of our bodies again, including expelling processed food, radiating heat and exhaling carbon dioxide. A lot of human progress has come from better strategies for solving our discharge needs, such as public sanitation. For fans of science fiction, like myself, dealing with the problems of discharge is an interesting limit on our ability to cloak ourselves.

Temperature. Our bodies can self regulate their temperature within a limited range. We have a need to control our environment to help our bodies with temperature regulation. Common strategies to meet our temperature needs include shelter and clothing.

Healing. When we damage our body in some fashion it needs to heal. The human body comes equipped with extensive systems for self healing including combating many foreign substances (including vomiting, diarrhea, antibodies). Beyond a certain range, the body needs external assistance to heal. Here to we have developed many solutions, and often group them under the term healthcare.

Learning. We are born quite, well, stupid. We even have to learn relatively basic skills such as walking and the use of tools. We group many of the strategies for solving the need for learning under the heading education. 

Recognition and Meaning. As humans we appear to have profound psychological needs for recognition and meaning in our lives. Religion and religious beliefs have long been a key strategy for solving this need. As I have argued in the section on Humanism, there is an objective basis for human meaning rooted in knowledge. Another key strategy to solve this need comes from our interactions with other humans, such as having someone acknowledge our contributions to a project. 


## Collective Needs

Reproduction. Individuals can survive without sex, but reproduction is a need for humanity as a whole. As humanity we have already learned how to solve the need for reproduction without sex. In the future there may be altogether different solutions for reproduction in the sense of the continuation of the human species.

Transportation. We need to bring ourselves and objects to different locations. In addition to our ability to walk and carry things, We have developed many different technological strategies for solving this need such as trains, planes and automobiles. Interestingly we are also developing new strategies that do not involve physical movement but use information movement instead, such as teleconferencing and sending digital representations of objects that can be locally manufactured.

Coordination. Whenever there is more than a single human involved in any activity, there is a need for coordination among the participating humans. We have developed many different communication and governance mechanisms to address this need.

Energy. To power all of our human activities we need energy. Our core strategy for energy meets for some time has been the burning of fossil fuels. We are in the midst of an important shift away from those and towards solar, wind and other renewable and cleaner energy sources. 

Raw materials. Everything we make requires raw materials as inputs. We have used strategies such as mining and chemistry to solve this need. Recycling is another strategy which recovers the raw materials from objects that are no longer needed.

Knowledge. As I have argued in the prior sections on Optimism and Humanism, this is the central collective human need. Without increased knowledge we will encounter problems that we cannot solve.


This is my current working version of needs. It represents a fairly substantial re-write from the previous version and I am much happier with the current one.

As you think about this list please keep in mind that I am not suggesting in this book that governments should directly provide for individual needs, for instance through a government run food program. Not even our collective needs necessarily require governments in their present forms. Instead, the point of having this list is to deliver on my claim that capital is no longer the scarce factor in meeting these needs.
